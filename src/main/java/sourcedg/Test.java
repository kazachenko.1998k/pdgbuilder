package sourcedg;

import sourcedg.builder.PDGBuilder;
import sourcedg.builder.PDGBuilderConfig;
import sourcedg.graph.*;
import sourcedg.util.GraphExporter;

import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;

public class Test {

    public static void main(final String[] args) throws Exception {
        File file = new File("C:\\Users\\Konstantin\\Desktop\\sourcedg-master\\src\\main\\java\\sourcedg\\ifTest.java");
        final FileInputStream in = new FileInputStream(file);
        PDGBuilderConfig config = PDGBuilderConfig.create();
        final PDGBuilder builder = new PDGBuilder(config, Level.ALL);
        builder.build(in);
        final PDG pdg = builder.getPDG();
        pdg.collapseNodes(VertexType.ACTUAL_OUT);
        for (int index = 0; index < pdg.vertexSet().size(); index++) {
            Vertex vertex = (Vertex) pdg.vertexSet().toArray()[index];
            if (
                    vertex.getType() == VertexType.CLASS ||
                            vertex.getType() == VertexType.FORMAL_IN ||
                            vertex.getType() == VertexType.FORMAL_OUT) {
                pdg.removeVertex(vertex);
            }
        }
        final Iterator<CFG> it = builder.getCfgs().iterator();

        CFG result = new CFG();
        while (it.hasNext()) {
            CFG cfg = it.next();
            for (int index = 0; index < cfg.vertexSet().size(); index++) {
                Vertex vertex = (Vertex) cfg.vertexSet().toArray()[index];
                result.addVertex(vertex);
            }
            for (int index = 0; index < cfg.edgeSet().size(); index++) {
                Edge edge = (Edge) cfg.edgeSet().toArray()[index];
                result.addEdge(edge.getSource(), edge.getTarget(), edge);
            }
        }
        for (int index = 0; index < pdg.edgeSet().size(); index++) {
            Edge edge = (Edge) pdg.edgeSet().toArray()[index];
            if (edge.getType() == EdgeType.DATA) {
                result.addEdge(edge.getSource(), edge.getTarget(), edge);
            }
        }

        GraphExporter.exportAsDot(result, "C:\\Users\\Konstantin\\Desktop\\sourcedg-master\\src\\main\\java\\sourcedg", "pdg");
    }

}
