package sourcedg;

public class ifTest {
    int fib(int n) {
        if (n < 2) {
            return n;
        } else {
            return fibCount(n);
        }
    }

    int fibCount(int n) {
        int a = 0, b = 1, i = 1, c = 0;
        while (i < n) {
            c = a + b;
            a = b;
            b = c;
            i++;
        }
        return c;
    }

}