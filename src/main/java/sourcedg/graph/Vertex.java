package sourcedg.graph;

import com.github.javaparser.ast.Node;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Vertex implements Serializable {
    private static final long serialVersionUID = -8461510336107098420L;
    private final transient Set<Vertex> in;
    private final Set<String> subtypes;
    private long id;
    private final String label;
    private String subLabel = "";
    private String assignment;
    private String submission;
    private VertexType type;
    private Integer startLine;
    private Integer endLine;
    private Integer line;
    private transient PDG pdg;
    private transient String def;
    private transient Set<String> uses;
    // Used for output dependence edges.
    private transient String pseudoUse;
    private transient Set<Vertex> out;
    private transient Node ast;

    // Graphviz attributes
    private String fillColor;

    private boolean visited;

    public Vertex(final String label) {
        this.label = label.replaceAll("\n", " ");
        uses = new HashSet<>();
        subtypes = new HashSet<>();
        in = new HashSet<>();
        out = new HashSet<>();
    }

    public Vertex(final VertexType type, final String label, final Node ast) {
        id = -1;
        this.type = type;
        this.label = label.replaceAll("\n", " ");
        uses = new HashSet<>();
        subtypes = new HashSet<>();
        in = new HashSet<>();
        out = new HashSet<>();
        this.ast = ast;
    }

    public Vertex(final VertexType type, final String label) {
        this(-1, type, label);
    }

    public Vertex(final long id, final VertexType type, final String label) {
        this.id = id;
        this.type = type;
        this.label = label.replaceAll("\n", " ");
        uses = new HashSet<>();
        subtypes = new HashSet<>();
        in = new HashSet<>();
        out = new HashSet<>();
    }

    public long getId() {
        return id;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public String getSubLabel() {
        return subLabel;
    }

    public void setSubLabel(String label) {
        this.subLabel = label;
    }

    public String getDef() {
        return def;
    }

    public void setDef(final String def) {
        this.def = def;
    }

    public Set<String> getUses() {
        if (uses == null)
            uses = new HashSet<>();
        return uses;
    }

    public void setUses(final Set<String> uses) {
        this.uses = uses;
    }

    public String getPseudoUse() {
        return pseudoUse;
    }

    public void setPseudoUse(String pseudoUse) {
        this.pseudoUse = pseudoUse;
    }

    public Set<Vertex> getIn() {
        return in;
    }

    public Set<Vertex> getOut() {
        return out;
    }

    public void setOut(final Set<Vertex> out) {
        this.out = out;
    }

    public VertexType getType() {
        return type;
    }

    public void setType(final VertexType type) {
        this.type = type;
    }

    public Set<String> getSubtypes() {
        return subtypes;
    }


    public Node getAst() {
        return ast;
    }

    public String getFillColor() {
        return fillColor;
    }

    public void clearDefUses() {
        def = null;
        uses = new HashSet<>();
    }

    public void clearUses() {
        uses = new HashSet<>();
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(final Integer line) {
        this.line = line;
    }

    @Override
    public String toString() {
        String result = id + "-" + type;
        if (label != null && label != "")
            result += "-" + label;
        return result;
    }

}
