package sourcedg.graph;

import org.jgrapht.graph.DirectedPseudograph;

import java.util.*;


public class PDG extends DirectedPseudograph<Vertex, Edge> {

    /**
     *
     */
    private static final long serialVersionUID = 4208713030783810103L;

    private static final Set<VertexType> dontCareTypes;

    static {
        dontCareTypes = new HashSet<>();
        dontCareTypes.add(VertexType.ACTUAL_IN);
        dontCareTypes.add(VertexType.ACTUAL_OUT);
        dontCareTypes.add(VertexType.ARRAY_IDX);
        dontCareTypes.add(VertexType.CLASS);
        dontCareTypes.add(VertexType.FORMAL_IN);
        dontCareTypes.add(VertexType.FORMAL_OUT);
        dontCareTypes.add(VertexType.ENTRY);
        dontCareTypes.add(VertexType.INIT);
        dontCareTypes.add(VertexType.TRY);
        dontCareTypes.add(VertexType.CATCH);
        dontCareTypes.add(VertexType.FINALLY);
    }

    public PDG() {
        super(Edge.class);
    }

    public void collapseNodes(VertexType type) {
//        if (!(type.equals(VertexType.ACTUAL_IN) || type.equals(VertexType.ACTUAL_OUT)
//                || type.equals(VertexType.ARRAY_IDX)))
//            throw new IllegalArgumentException();

        Set<Edge> removeEdges = new HashSet<>();
        Set<Vertex> removeNodes = new HashSet<>();
        for (Vertex v : this.vertexSet()) {
            if (v.getType().equals(type)) {
                Edge edge = this.incomingEdgesOf(v).stream().filter(e -> e.getType().equals(EdgeType.CTRL_TRUE))
                        .findFirst().get();
                Vertex parent = edge.getSource();

                Set<Edge> in = new HashSet<>(this.incomingEdgesOf(v));
                Set<Edge> out = this.outgoingEdgesOf(v);

                removeNodes.add(v);
                removeEdges.addAll(in);
                removeEdges.addAll(out);

                in.remove(edge);
                for (Edge e : in) {
                    Edge newEdge = new Edge(e.getSource(), parent, e.getType());
                    this.addEdge(e.getSource(), parent, newEdge);
                }

                for (Edge e : out) {
                    Edge newEdge = new Edge(parent, e.getTarget(), e.getType());
                    this.addEdge(parent, e.getTarget(), newEdge);
                }
                parent.getSubtypes().addAll(v.getSubtypes());

            }
        }
        this.removeAllEdges(removeEdges);
        this.removeAllVertices(removeNodes);

        // Update IDs of nodes.
        List<Vertex> sortedNodes = new ArrayList<>(this.vertexSet());
        Collections.sort(sortedNodes, Comparator.comparing(v -> v.getId()));
        int id = 0;
        for (Vertex v : sortedNodes) {
            v.setId(id++);
        }
    }

    public Vertex actualOut(final Vertex v) {
        final Set<Edge> edges = outgoingEdgesOf(v);
        Vertex successor = null;
        for (final Edge e : edges) {
            successor = e.getTarget();
            if (VertexType.ACTUAL_OUT.equals(successor.getType())) {
                return successor;
            }
        }
        return null;
    }

}
