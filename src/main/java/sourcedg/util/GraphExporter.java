package sourcedg.util;

import org.jgrapht.Graph;
import org.jgrapht.io.*;
import sourcedg.graph.Edge;
import sourcedg.graph.Vertex;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class GraphExporter {

    private static final ComponentNameProvider<Vertex> vertexLabelProvider = new ComponentNameProvider<Vertex>() {
        @Override
        public String getName(final Vertex component) {
            String sub = component.getSubLabel();
            if (!sub.isEmpty()) {
                sub += ".";
            }
            System.out.println(component.getSubLabel());
            String result = sub;
            final Set<String> subtypes = component.getSubtypes();
            // if (!subtypes.isEmpty())
            // result += subtypes + "\n";
            result += component.getLabel().replaceAll("\"", "'");
            return result;
        }
    };


    private static final ComponentAttributeProvider<Vertex> vertexAttrProvider = new ComponentAttributeProvider<Vertex>() {

        @Override
        public Map<String, Attribute> getComponentAttributes(final Vertex component) {
            final Map<String, Attribute> result = new HashMap<>();
            final Attribute fillColor = DefaultAttribute.createAttribute(component.getFillColor());
//			if (fillColor != null) {
//				result.put("style", DefaultAttribute.createAttribute("filled"));
//				result.put("fillcolor", fillColor);
//			}
            if (component.getType() != null) {
                switch (component.getType()) {
                    case ENTRY:
                    case RETURN:
                        result.put("shape", DefaultAttribute.createAttribute("oval"));
                        break;
                    case CTRL:
                        result.put("shape", DefaultAttribute.createAttribute("diamond"));
                        break;
                    default:
                        result.put("fontname", DefaultAttribute.createAttribute("helvetica"));
                        result.put("shape", DefaultAttribute.createAttribute("rectangle"));
                        break;
                }
            }
            return result;
        }
    };

    private static final ComponentAttributeProvider<Edge> edgeAttrProvider = new ComponentAttributeProvider<Edge>() {

        @Override
        public Map<String, Attribute> getComponentAttributes(final Edge component) {
            final Map<String, Attribute> result = new HashMap<>();
            if (component.getType() != null) {
                switch (component.getType()) {
                    case CTRL_TRUE:
                        result.put("color", DefaultAttribute.createAttribute("black"));
                        break;
                    case DATA:
                        result.put("style", DefaultAttribute.createAttribute("dotted"));
                        result.put("color", DefaultAttribute.createAttribute("blue"));
                        break;
                    case OUTPUT:
                        result.put("style", DefaultAttribute.createAttribute("bold"));
                        result.put("color", DefaultAttribute.createAttribute("yellow"));
                        break;
                    case CALL:
                    case PARAM_IN:
                    case PARAM_OUT:
                        result.put("style", DefaultAttribute.createAttribute("dashed"));
                        result.put("color", DefaultAttribute.createAttribute("red"));
                        result.put("constraint", DefaultAttribute.createAttribute(false));
                        break;
                    default:
                        break;
                }
            }
            return result;
        }
    };

    private static final DOTExporter<Vertex, Edge> exporter = new DOTExporter<>(new IntegerComponentNameProvider<>(),
            vertexLabelProvider, null, vertexAttrProvider, edgeAttrProvider);

    public static void exportAsDot(final Graph<Vertex, Edge> graph, final String path, final String fileName) {
        try {
            final String filePath = path + "/" + fileName + ".dot";
            final File dotFile = new File(filePath);
            exporter.exportGraph(graph, dotFile);
        } catch (final ExportException e) {
            e.printStackTrace();
        }
    }
}
